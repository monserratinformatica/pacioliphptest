<?php

include('./httpful.phar');

use \Httpful\Request;

class Detalle {
	public $Cantidad;
	public $Exento;
	public $Monto;
	public $NombreItem;
	public $PorcentajeDescuento;
	public $Precio;
	public $TipoCodigo;
	public $ValorCodigo;
	public $UnidadMedida;
}

class Empresa {
	public $RazonSocial;
	public $RUT;
	public $Giro;
	public $Direccion;
	public $Comuna;
	public $Ciudad;
}

class Factura {
	public $FormaDePago;
	public $Receptor;
	public $Detalle = array();
	
}

$detalle = new Detalle();
$detalle->Cantidad = 1;
$detalle->Precio = 100;
$detalle->NombreItem = "Factura desde PHP";
$detalle->Monto = $detalle->Cantidad * $detalle->Precio;
$detalle->PorcentajeDescuento = 0;
$detalle->Exento = false;

$empresa = new Empresa();
$empresa->RazonSocial = "Rubén Cañihua";
$empresa->RUT = 12345679;
$empresa->Giro = "Giro de Prueba";
$empresa->Direccion = "Dirección de prueba";
$empresa->Comuna = "Huechuraba";
$empresa->Ciudad = "Santiago";

$factura = new Factura();
$factura->FormaDePago = "Al contento";
$factura->Receptor = $empresa;
$factura->Detalle[0] = $detalle;


$json = json_encode($factura);

$url = "http://localhost:28933/api/FacturaElectronica";
$response = Request::post($url)
    ->sendsJson()
    ->body($json)
	->send();

if ($response->code == 200) {
	echo "Nueva factura: {$response->body->Id} - Folio: <a href=\"{$url}/GetPDF/33/{$response->body->Folio}\">{$response->body->Folio}</a>";
} else {
	echo "Error: {$response->body->Message}";
	if (property_exists($response->body, "ModelState")) {
		foreach ($response->body->ModelState as $prop => $lst) {
			echo "<br><b>{$prop}</b>:<ul>";
			foreach ($lst as $err) {
				echo "<li>{$err}</li>";
			}
			echo "</ul>";
		}
	}
}

?>
