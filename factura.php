<?php

include('./httpful.phar');

use \Httpful\Request;

require_once('.\clases.php');

$detalle = new Detalle();
$detalle->Cantidad = 1;
$detalle->Precio = 100;
$detalle->NombreItem = "Factura desde PHP";
$detalle->Monto = $detalle->Cantidad * $detalle->Precio;
$detalle->PorcentajeDescuento = 0;
$detalle->Exento = false;

$empresa = new Empresa();
$empresa->RazonSocial = p('nombre');
$empresa->RUT = p('rut');
$empresa->Giro = p('giro');
$empresa->Direccion = p('direccion');
$empresa->Comuna = "Huechuraba";
$empresa->Ciudad = "Santiago";

$factura = new Factura();
$factura->FormaDePago = "Contado";
$factura->Receptor = $empresa;
$factura->Detalle[0] = $detalle;


$json = json_encode($factura);

$url = "http://localhost:28933/api/FacturaElectronica";
$response = Request::post($url)
    ->sendsJson()
    ->body($json)
	->send();

if ($response->code == 200) {
	echo "Nueva factura: {$response->body->Id} - Folio: <a href=\"{$url}/GetPDF/33/{$response->body->Folio}\">{$response->body->Folio}</a>";
} else {
	// echo var_dump($response);
	echo "Error: {$response->body->Message}<dl>";
	foreach ($response->body->ModelState as $key => $value) {
		echo "<dt>{$key} : <dt><dd><ul>";
		for ($i = 0; $i < count($value); $i++) {
			echo "<li>{$value[$i]}</li>";
		}
		echo "</ul></dd>";
	}
	echo "</dl>";
}

?>
