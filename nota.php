<?php

include('./httpful.phar');

use \Httpful\Request;

require_once('.\clases.php');

$detalle = new Detalle();
$detalle->Cantidad = 1;
$detalle->Precio = 20;
$detalle->NombreItem = "Factura desde PHP";
$detalle->Monto = $detalle->Cantidad * $detalle->Precio;
$detalle->PorcentajeDescuento = 0;
$detalle->Exento = false;

$empresa = new Empresa();
$empresa->RazonSocial = 'x';
$empresa->RUT = p('rut');
$empresa->Giro = p('giro');
$empresa->Direccion = p('direccion');
$empresa->Comuna = "Huechuraba";
$empresa->Ciudad = "Santiago";

$nota = new Nota();
$nota->Receptor = $empresa;
$nota->MotivoNota = p('motivo');
$nota->FolioRef = p('folioref');
$nota->TipoDocRef  = 33;

if ($nota->Motivo = 3) {
	$nota->Detalle[0] = $detalle;
}

$json = json_encode($nota);

$url = "http://localhost:28933/api/NotaCredito";
$response = Request::post($url)
    ->sendsJson()
    ->body($json)
	->send();

if ($response->code == 200) {
	echo "Nueva nota: {$response->body->Id} - Folio: <a href=\"{$url}/GetPDF/61/{$response->body->Folio}\">{$response->body->Folio}</a>";
} else {
	echo "Error";
	echo var_dump($response);
}

?>
