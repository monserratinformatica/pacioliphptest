<?php
class Detalle {
	public $Cantidad;
	public $Exento;
	public $Monto;
	public $NombreItem;
	public $PorcentajeDescuento;
	public $Precio;
	public $TipoCodigo;
	public $ValorCodigo;
	public $UnidadMedida;
}

class Empresa {
	public $RazonSocial;
	public $RUT;
	public $Giro;
	public $Direccion;
	public $Comuna;
	public $Ciudad;
	public $Telefono;
}

class Factura {
	public $FormaDePago;
	public $Receptor;
	public $Comentarios;
	public $Detalle = array();
}

class Nota {
	public $FolioRef;
	public $TipoDocRef;
	public $MotivoNota;
}


function p($nombre)
{
	 if (array_key_exists($nombre, $_POST)) {
		 return $_POST[$nombre];
	 }
	 die("Valor '$nombre' no encontrado en formulario");
}

?>